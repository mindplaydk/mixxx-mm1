function CMD() {};

CMD.vumeter = {};
CMD.vumeter[1] = 0;
CMD.vumeter[2] = 0;

CMD.init = function (id) {
	engine.connectControl("[Master]", "VuMeterL", "CMD.VuMeterL");
	engine.connectControl("[Master]", "VuMeterR", "CMD.VuMeterR");
}

CMD.shutdown = function () {}

CMD.update_vumeter = function (channel, value)
{
	var newval = parseInt(value * 0x10);
	
	if (CMD.vumeter[channel] != newval) {
		CMD.vumeter[channel] = newval;

		midi.sendShortMsg(180, 80 + channel, 48 + newval);
	}
}

CMD.VuMeterL = function (value)
{
	if (CMD.vumeter_master_mode == false) return;
	
	CMD.update_vumeter(1, value);
}

CMD.VuMeterR = function (value)
{
	if (CMD.vumeter_master_mode == false) return;
	
	CMD.update_vumeter(2, value);
}
